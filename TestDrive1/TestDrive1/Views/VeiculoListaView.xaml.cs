﻿using Ifrn.TestDrive.Models;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Ifrn.TestDrive.Views
{
    public partial class VeiculoListaView : ContentPage
	{

        public List<Veiculo> Veiculos { get; set; }

		public VeiculoListaView()
		{
			InitializeComponent();

            this.Veiculos = new List<Veiculo>();

            Veiculos.Add(new Veiculo("Celta", 29000));
            Veiculos.Add(new Veiculo("Corsa", 37000));
            Veiculos.Add(new Veiculo("Palio", 40000));
            Veiculos.Add(new Veiculo("Prisma", 46000));
            Veiculos.Add(new Veiculo("Gol", 40000));

            this.BindingContext = this;
        }

        private void VeiculoSelecionado(object sender, ItemTappedEventArgs e)
        {
            var veiculo = (Veiculo)e.Item;

            string titulo = veiculo.Nome;
            string mensagem = string.Format("Preço: {0}", veiculo.PrecoFormatado);

            DisplayAlert(titulo, mensagem, "Ok");
        }
    }
}
