﻿using System.Globalization;

namespace Ifrn.TestDrive.Models
{
    public class Veiculo
    {
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public string PrecoFormatado
        {
            get
            {
                return string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", Preco);
            }
        }

        public Veiculo(string Nome, decimal Preco)
        {
            this.Nome = Nome;
            this.Preco = Preco;
        }
    }
}
